<?php
/**
 * Template Name: Contact
 * @package  WordPress
 * @subpackage  Timber
 * @since   Timber 0.1
*/

$context = Timber::get_context();
$post = Timber::get_post();
$context['post'] = $post;

// $context['contact_form'] = gravity_form( 1, false, false, false, '', false, 4, false );

$templates = [ 'contact.twig' ];

Timber::render( $templates, $context );