<?php
// This functions file is for all custom blocks added via ACF
// Reference: https://www.advancedcustomfields.com/resources/acf_register_block_type/

if( function_exists('acf_register_block_type') ) :
	include 'acf-blocks-callback.php'; // pass-off to let Timber render the blocks

	/** Blocks **/
	$dropdown_block = [
		'name' => 'dropdown-block',
		'title' => __( 'Dropdown Block', 'blp' ),
		'description' => __( 'Creates a dropdown container; The content is folded into the dropdown title.', 'blp' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'blp-blocks',
		'align' => 'wide',
		'icon' => 'sort',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'drop', 'down', 'dropdown', 'accordion' ]
	];
	acf_register_block_type( $dropdown_block );

	$gallery_block = [
		'name' => 'blp-gallery',
		'title' => __( 'Image Gallery', 'blp' ),
		'description' => __( 'Creates a 5 column image gallery in a grid.\n Clicking an image will open a modal, where the image can be viewed at its normal size.', 'blp' ),
		'render_callback' => 'acf_custom_blocks_callback',
		'category' => 'blp-blocks',
		'align' => 'center',
		'icon' => 'images-alt',
		'mode' => 'auto',
		'supports' => [ 'mode' => true ],
		'keywords' => [ 'gallery', 'image', 'grid', 'modal' ]
	];
	acf_register_block_type( $gallery_block );

	// $green_button_block = [
	// 	'name' => 'green-button-block',
	// 	'title' => __( 'Green Button', 'blp' ),
	// 	'description' => __( 'Creates a Green button with white text.', 'blp' ),
	// 	'render_callback' => 'acf_custom_blocks_callback',
	// 	'category' => 'blp-blocks',
	// 	'align' => 'center',
	// 	'icon' => 'editor-removeformatting',
	// 	'mode' => 'auto',
	// 	'supports' => [ 'mode' => true ],
	// 	'keywords' => [ 'button', 'anchor', 'green' ]
	// ];
	// acf_register_block_type( $green_button_block );

	// $purple_button_block = [
	// 	'name' => 'purple-button-block',
	// 	'title' => __( 'Purple Button', 'blp' ),
	// 	'description' => __( 'Creates a Purple button with white text.', 'blp' ),
	// 	'render_callback' => 'acf_custom_blocks_callback',
	// 	'category' => 'blp-blocks',
	// 	'align' => 'center',
	// 	'icon' => 'editor-removeformatting',
	// 	'mode' => 'auto',
	// 	'supports' => [ 'mode' => true ],
	// 	'keywords' => [ 'button', 'anchor', 'purple' ]
	// ];
	// acf_register_block_type( $purple_button_block );
endif;