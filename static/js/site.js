(function($) {
    $(document).ready(function() {
		let $body = $('body');

		// mobile menu open/close functions
		$(function siteNavigation() {
			$('#menu-toggle').click(function(e) {
				e.preventDefault(); // prevents scrollTop()
				$('.x-bar').toggleClass('x-bar-active');
				$('#primary-menu').slideToggle(550);
			});

			// make sure we are on mobile since we are targeting classes
			// dropdown function for mobile menu
			if( $body.width() < 850 ) {
				$('.menu-item-has-children').click(function() {
					var $sub = $(this).find('.sub-menu:first');
					$sub.slideToggle(550);
				});
			}
		});

		// dropdown block - controls and aria events for screenreaders
		$(function dropDownBlock() {
			$('.dropdown-content').each(function() {
				$(this).hide();
			});

			$('.dropdown-block-title').click(function() {
				var $this = $(this);
				// fires on first click (content is expanded)
				if( $this.hasClass('target') ) {
					$this.removeClass('target');
					$this.next('.dropdown-content').slideToggle(700);
					$this.attr('aria-pressed', 'true');
					$this.next('.dropdown-content').attr('aria-expanded', 'true');
				} else {
					// fires on second click (content is closed)
					$this.next('.dropdown-content:first').slideToggle(700, function() {
						$this.prev('.dropdown-block-title').addClass('target');
						$this.prev('.dropdown-block-title').attr('aria-pressed', 'false');
						$this.attr('aria-expanded', 'false');
					});
				}
				// always fire
				$(this).toggleClass('chevron-rotate');
			});
		});
	}); // end Document.Ready

	// Facet Reset Button (both seed pages utilize same template)
	$(document).on('facetwp-loaded', function() {
		var qs = FWP.build_query_string();
		if ( '' === qs ) { // no facets are selected
			$('.reset-button').hide();
		} else {
			$('.reset-button').show();
		}

		// hide dealer post type initially on load (only applied to dealers - other posts are visible)
		// when the user chooses a dealer facet value, load the dealer grid(s)
		if( FWP.loaded ) {
			$('.facetwp-template').addClass('visible');
		}
	});
})(jQuery);